# frozen_string_literal: true

# Copyright (c) 2019 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'chef/knife'

class Chef
  class Knife
    # EnvironmentSync class
    class EnvironmentSync < Chef::Knife
      deps do
        require 'chef/environment'
        require 'chef/knife/core/object_loader'
        require 'hashdiff'
      end

      banner 'knife environment sync [ENVIRONMENT] (options)'
      category 'environment'

      option :diff,
             long: '--diff',
             description: 'Show differences between items'

      option :abbrev,
             long: '--abbrev',
             description: 'Abbrev diff path as string instead of array'

      def loader
        @loader ||= Core::ObjectLoader.new(Chef::Environment, ui)
      end

      def compare(env, chef_env, local_env)
        msg = "--> Checking #{env}".ljust(50)
        opts = config[:abbrev] ? {} : { array_path: true }
        diff = ::Hashdiff.diff(chef_env, local_env, opts)
        diff.empty? ? show_success(msg) : show_error(msg, diff)
      end

      def find_env_file(env)
        config_path = Chef::Config[:environment_path]
        env_paths = config_path.is_a?(String) ? [config_path] : config_path
        env_paths.map do |env_path|
          path_glob =
            File.join(Chef::Util::PathHelper.escape_glob_dir(env_path), '**')
          path_glob << "/#{env.gsub(/[_-]/, '{-,_}')}.{json,rb}"
          Dir.glob(path_glob)
        end.flatten
      end

      def env_from_chef(env)
        Chef::Environment.load(env).to_hash
      rescue Net::HTTPServerException
        ui.error("Environment '#{env}' could not be found on chef server")
      end

      def env_from_file(env)
        files = find_env_file(env.tr('-', '_'))
        if files.empty?
          ui.error("Environment '#{env}' could not be found on disk")
        elsif files.count > 1
          ui.error("Duplicate file environment for '#{env}'")
        else
          loader.object_from_file(files.first).to_hash
        end
      end

      def run
        envs = name_args.empty? ? Chef::Environment.list.keys : name_args
        envs.delete('_default')
        envs.each do |env|
          chef_env = env_from_chef(env)
          local_env = env_from_file(env)
          next if chef_env.nil? || local_env.nil?

          compare(env, chef_env, local_env)
        end
      end

      def show_success(msg)
        ui.info(msg + ui.color('properly synchronized.', :green))
      end

      def show_error(msg, diff)
        ui.info(msg + ui.color('difference(s) found', :yellow))
        pp(diff) if config[:diff]
      end
    end
  end
end
