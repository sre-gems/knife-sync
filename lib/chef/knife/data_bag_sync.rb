# frozen_string_literal: true

# Copyright (c) 2019 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'chef/knife'

class Chef
  class Knife
    # DataBagSync class
    # rubocop:disable  Metrics/ClassLength
    class DataBagSync < Chef::Knife
      include DataBagSecretOptions

      deps do
        require 'chef/data_bag'
        require 'chef/data_bag_item'
        require 'chef/encrypted_data_bag_item'
        require 'chef/knife/core/object_loader'
        require 'chef/knife/data_bag_secret_options'
        require 'hashdiff'
        require 'find'
      end

      banner 'knife data bag sync [BAG] [ITEM] (options)'
      category 'data bag'

      option :diff,
             long: '--diff',
             description: 'Show differences between items'

      option :abbrev,
             long: '--abbrev',
             description: 'Abbrev diff path as string instead of array'

      def loader
        @loader ||= Core::ObjectLoader.new(Chef::DataBagItem, ui)
      end

      def compare(chef_data_bag, local_data_bag)
        chef_data_bag.each do |item, data|
          if local_data_bag.key?(item)
            compare_item(item, data, local_data_bag[item])
          else
            ui.error("Data bag item '#{item}' could not be locally found")
          end
        end
      end

      def compare_item(item, chef_item, local_item)
        msg = "Item '#{item}':".ljust(50)
        opts = config[:abbrev] ? {} : { array_path: true }
        diff = ::Hashdiff.diff(chef_item, local_item, opts)
        diff.empty? ? show_success(msg) : show_error(msg, diff)
      end

      def data_bag_from_chef(data_bag)
        items = find_chef_data_bag_items(data_bag)
        if items.empty?
          ui.error("No items founded in chef data bag '#{data_bag}'")
        end
        items
      rescue Net::HTTPServerException
        ui.error("Data bag '#{data_bag}' could not be found on chef server")
        {}
      end

      def data_bag_path
        config_path = Chef::Config[:data_bag_path]
        config_path.is_a?(String) ? [config_path] : config_path
      end

      # rubocop:disable Metrics/MethodLength
      def data_bag_from_file(data_bag, list)
        items = nil
        data_bag_path.map do |data_bag_path|
          path = File.join(data_bag_path, data_bag)
          next unless File.directory?(path)

          items = find_local_data_bag_items(path, list)
          if items.empty?
            ui.error("No items founded in local data bag '#{data_bag}'")
          end
        end

        if items.nil?
          ui.error("Data bag '#{data_bag}' could not be locally found")
          items = {}
        end
        items
      end
      # rubocop:enable Metrics/MethodLength

      def data_bag_item_from_chef(data_bag, item)
        data_bag_item = Chef::DataBagItem.load(data_bag, item).raw_data
        if encrypted?(data_bag_item) && secret
          data_bag_item =
            Chef::EncryptedDataBagItem.load(data_bag, item, secret).to_h
        end
        data_bag_item
      rescue Net::HTTPServerException
        ui.error("Item '#{item}' could not be found on chef server")
        nil
      end

      def data_bag_item_from_file(data_bag_path, item)
        path = Pathname.new(data_bag_path)
        data = loader.load_from(path.parent, File.basename(path), item)
        encrypted?(data) ? decrypt(item, data) : data
      rescue SystemExit
        ui.error("Data bag item #{item} could not be loaded")
        nil
      end

      def decrypt(name, data)
        if secret
          Chef::EncryptedDataBagItem.new(data, secret).to_hash
        else
          ui.warn(
            "Data bag item '#{name}' is encrypted, but no secret provided " \
            'for decoding (will be ignored)'
          )
          nil
        end
      end

      def find_chef_data_bag_items(data_bag)
        Chef::DataBag.load(data_bag).to_hash.map do |item, _|
          [item, data_bag_item_from_chef(data_bag, item)]
        end.to_h
      end

      def find_local_data_bag_items(data_bag_path, list)
        items = {}
        ::Find.find(data_bag_path) do |path|
          item = File.basename(path, File.extname(path))
          if File.file?(path) && list.include?(item)
            items[item] = data_bag_item_from_file(data_bag_path, path)
          end
        end
        items.compact
      end

      def run
        case @name_args.length
        when 0
          sync_data_bags(Chef::DataBag.list.keys)
        when 1
          sync_data_bags(@name_args)
        when 2
          sync_item(@name_args[0], @name_args[1])
        end
      end

      def secret
        encryption_secret_provided_ignore_encrypt_flag? ? read_secret : nil
      end

      def show_success(msg)
        ui.info(msg + ui.color('properly synchronized.', :green))
      end

      def show_error(msg, diff)
        ui.info(msg + ui.color('difference(s) found', :yellow))
        pp(diff) if config[:diff]
      end

      def sync_data_bags(data_bags)
        data_bags.each do |data_bag|
          ui.info("--> Checking data bag '#{data_bag}'")
          chef_data_bag = data_bag_from_chef(data_bag)
          msg = "No items founded in chef data bag '#{data_bag}'"
          next ui.error(msg) if chef_data_bag.empty?

          local_data_bag = data_bag_from_file(data_bag, chef_data_bag.keys)
          next if local_data_bag.empty?

          compare(chef_data_bag, local_data_bag)
        end
      end

      def sync_item(data_bag, item)
        ui.info("-> Checking item '#{item}' from data bag '#{data_bag}'")
        chef_item = data_bag_item_from_chef(data_bag, item)
        local_item = data_bag_from_file(data_bag, [item])
        if local_item.empty?
          ui.error("Data bag item '#{item}' could not be locally found")
        else
          compare_item(item, chef_item, local_item[item])
        end
      end
    end
  end
  # rubocop:enable  Metrics/ClassLength
end
