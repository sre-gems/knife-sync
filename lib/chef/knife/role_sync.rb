# frozen_string_literal: true

# Copyright (c) 2019 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'chef/knife'

class Chef
  class Knife
    # RoleSync class
    class RoleSync < Knife
      deps do
        require 'chef/role'
        require 'chef/knife/core/object_loader'
        require 'hashdiff'
      end

      banner 'knife role sync [ROLE] (options)'
      category 'role'

      option :diff,
             long: '--diff',
             description: 'Show differences between items'

      option :abbrev,
             long: '--abbrev',
             description: 'Abbrev diff path as string instead of array'

      def loader
        @loader ||= Knife::Core::ObjectLoader.new(Chef::Role, ui)
      end

      def compare(role, chef_role, local_role)
        msg = "--> Checking #{role}".ljust(50)
        opts = config[:abbrev] ? {} : { array_path: true }
        diff = ::Hashdiff.diff(chef_role, local_role, opts)
        diff.empty? ? show_success(msg) : show_error(msg, diff)
      end

      def find_role_file(role)
        config_path = Chef::Config[:role_path]
        role_paths = config_path.is_a?(String) ? [config_path] : config_path
        role_paths.map do |path|
          path_glob =
            File.join(Chef::Util::PathHelper.escape_glob_dir(path), '**')
          path_glob << "/#{role.gsub(/[_-]/, '{-,_}')}.{json,rb}"
          Dir.glob(path_glob)
        end.flatten
      end

      def role_from_chef(role)
        Chef::Role.load(role).to_hash
      rescue Net::HTTPServerException
        ui.error("Role '#{role}' could not be found on chef server")
      end

      def role_from_file(role)
        files = find_role_file(role)
        if files.empty?
          ui.error("Role '#{role}' could not be found on disk")
        elsif files.count > 1
          ui.error("Duplicate file role for '#{role}'")
        else
          loader.object_from_file(files.first).to_hash
        end
      end

      def run
        roles = name_args.empty? ? Chef::Role.list.keys : name_args
        roles.each do |role|
          chef_role = role_from_chef(role)
          local_role = role_from_file(role)
          next if chef_role.nil? || local_role.nil?

          compare(role, chef_role, local_role)
        end
      end

      def show_success(msg)
        ui.info(msg + ui.color('properly synchronized.', :green))
      end

      def show_error(msg, diff)
        ui.info(msg + ui.color('difference(s) found', :yellow))
        pp(diff) if config[:diff]
      end
    end
  end
end
