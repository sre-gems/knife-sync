# frozen_string_literal: true

# Copyright (c) 2019 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'chef/knife'

class Chef
  class Knife
    # NodeSync class
    class NodeSync < Knife
      deps do
        require 'chef/node'
        require 'chef/knife/core/object_loader'
        require 'hashdiff'
      end

      banner 'knife node sync [NODE] (options)'
      category 'node'

      option :diff,
             long: '--diff',
             description: 'Show differences between items'

      option :abbrev,
             long: '--abbrev',
             description: 'Abbrev diff path as string instead of array'

      def loader
        @loader ||= Core::ObjectLoader.new(Chef::Node, ui)
      end

      def compare(node, chef_node, local_node)
        msg = "--> Checking #{node}".ljust(50)
        chef_node = chef_node.select { |k, _| local_node.key?(k) }
        opts = config[:abbrev] ? {} : { array_path: true }
        diff = ::Hashdiff.diff(chef_node, local_node, opts)
        diff.empty? ? show_success(msg) : show_error(msg, diff)
      end

      def find_node_file(node)
        config_path = Chef::Config[:node_path]
        node_paths = config_path.is_a?(String) ? [config_path] : config_path
        node_paths.map do |node_path|
          path_glob =
            File.join(Chef::Util::PathHelper.escape_glob_dir(node_path), '**')
          path_glob << "/#{node.gsub(/[_-]/, '{-,_}')}.{json,rb}"
          Dir.glob(path_glob)
        end.flatten
      end

      def node_from_chef(node)
        Chef::Node.load(node).to_hash
      rescue Net::HTTPServerException
        ui.error("Node '#{node}' could not be found on chef server")
      end

      def node_from_file(node)
        files = find_node_file(node)
        if files.empty?
          ui.error("Node '#{node}' could not be found on disk")
        elsif files.count > 1
          ui.error("Duplicate file node for '#{node}'")
        else
          loader.object_from_file(files.first).to_hash
        end
      end

      def run
        nodes = name_args.empty? ? Chef::Node.list.keys : name_args
        nodes.each do |node|
          chef_node = node_from_chef(node)
          local_node = node_from_file(node)
          next if chef_node.nil? || local_node.nil?

          compare(node, chef_node, local_node)
        end
      end

      def show_success(msg)
        ui.info(msg + ui.color('properly synchronized.', :green))
      end

      def show_error(msg, diff)
        ui.info(msg + ui.color('difference(s) found', :yellow))
        pp(diff) if config[:diff]
      end
    end
  end
end
