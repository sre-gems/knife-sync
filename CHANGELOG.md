Changelog
=========

1.0.1
-----

- Gem yank command has been misused : version bump is necessary

1.0.0
-----

- Initial version
