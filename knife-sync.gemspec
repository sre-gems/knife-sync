# frozen_string_literal: true

# Copyright (c) 2019 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'knife-sync/version'

Gem::Specification.new do |spec|
  spec.name          = 'knife-sync'
  spec.version       = Knife::Sync::VERSION
  spec.authors       = ['Sylvain Arrambourg']
  spec.email         = ['incoming+sre-gems/knife-sync@incoming.gitlab.com']
  spec.license       = 'Apache-2.0'

  spec.summary       = 'Knife Sync Plugin'
  spec.description   = 'Knife Sync Plugin'
  spec.homepage      = 'https://gitlab.com/sre-gems/knife-sync'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'hashdiff', '~> 0.3.0'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'chef', '>= 13.0'
  spec.add_development_dependency 'rake', '~> 12.3'
  spec.add_development_dependency 'rspec', '~> 3'
  spec.add_development_dependency 'rubocop', '~> 0.65'
end
