Knife Sync
==========

Knife Sync is a plugin knife to check that your local chef-repo
and the Chef server are well synchronized.

It displays sync state of following Chef policies :
 - Nodes Run-list
 - Roles
 - Environments
 - Data Bags

Installation
------------

    $ gem install knife-sync

Usage
-----

This plugin adds the following set of subcommands:

    knife sync node [NODE] (options)
    knife sync role [ROLE] (options)
    knife sync environment [ENVIRONMENT] (options)
    knife sync role [BAG] [ITEM] (options)

Available options:

    --diff     Show differences between items
    --abbrev   Abbrev diff path as string instead of array

Release
-------

To Rubygems:

```
rake clobber
rake build
gem push pkg/knife-sync-$VERSION.gem
```

Contributing
------------

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

License and Author
------------------

- Author:: Sylvain Arrambourg (<saye@sknss.net>)

```text
Copyright (c) 2017-2019 Make.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
