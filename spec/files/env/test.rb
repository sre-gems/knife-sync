# frozen_string_literal: true

name 'test'
description 'environment for plugin test purpose'
cookbook_versions({})

default_attributes(
  'test' => { 'data' => %w[this is a test] }
)
