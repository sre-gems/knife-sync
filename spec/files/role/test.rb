# frozen_string_literal: true

name 'test'
description 'role for plugin test purpose'

run_list(
  'role[test]',
  'recipe[test]'
)

default_attributes(
  'test' => { 'data' => %w[this is a test] }
)
