# frozen_string_literal: true

require 'spec_helper'

require 'chef/data_bag'
require 'chef/data_bag_item'
require 'chef/encrypted_data_bag_item'
require 'chef/knife/core/object_loader'
require 'chef/knife/data_bag_secret_options'
require 'chef/knife/data_bag_sync'
require 'hashdiff'
require 'find'

# rubocop:disable Metrics/BlockLength
describe Chef::Knife::DataBagSync do
  Chef::Config[:data_bag_path] =
    [File.join(File.expand_path('..', __dir__), 'files', 'data_bag')]

  let(:data_bag) { 'test' }
  let(:data_bag_path) { Chef::Config[:data_bag_path].first }
  let(:items) { %w[item1 item2] }
  let(:instance) { described_class.new }

  secret_path =
    File.join(File.expand_path('..', __dir__), 'files', 'secret_key')
  Chef::Config[:encrypted_data_bag_secret] = secret_path
  Chef::Config[:knife][:cl_secret_file] = secret_path

  describe '#data_bag_from_chef' do
    subject { instance.data_bag_from_chef(data_bag) }

    context 'when data bag is found' do
      before do
        allow(instance).to receive('find_chef_data_bag_items')
          .with(data_bag) { data_bag_as_hash }
      end

      it 'return data bag as hash' do
        is_expected.to eq(data_bag_as_hash)
      end
    end

    context 'when data bag is empty' do
      before do
        allow(instance) .to receive('find_chef_data_bag_items')
          .with(data_bag) { {} }
      end

      it 'return empty hash and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_empty
      end
    end

    context 'when data bag is not found' do
      let(:data_bag) { 'absent' }

      before do
        allow(Chef::DataBag).to receive('load')
          .with(data_bag)
          .and_raise(Net::HTTPServerException.new('stub', Net::HTTPNotFound))
      end

      it 'return empty hash and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_empty
      end
    end
  end

  describe '#data_bag_from_file' do
    subject { instance.data_bag_from_file(data_bag, items) }

    context 'when data bag is found' do
      it 'return data bag as hash' do
        is_expected.to eq(data_bag_as_hash)
      end
    end

    context 'when data bag is empty' do
      before do
        allow(instance).to receive('find_local_data_bag_items') { {} }
      end

      it 'return empty hash and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_empty
      end
    end

    context 'when data bag is not found' do
      let(:data_bag) { 'absent' }

      it 'return empty hash and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_empty
      end
    end
  end

  describe '#find_chef_data_bag_items' do
    subject { instance.find_chef_data_bag_items(data_bag) }

    context 'when data bag is found' do
      before do
        allow(Chef::DataBag).to receive('load')
          .with(data_bag).and_return(data_bag_as_hash)
        allow(instance).to receive(:data_bag_item_from_chef)
          .with(data_bag, items.first)
          .and_return(data_bag_as_hash[items.first])
        allow(instance).to receive(:data_bag_item_from_chef)
          .with(data_bag, items.last)
          .and_return(data_bag_as_hash[items.last])
      end

      it 'return data bag items as hash' do
        is_expected.to eq(data_bag_as_hash)
      end
    end

    context 'when data bag is not found' do
      let(:data_bag) { 'absent' }

      before do
        allow(Chef::DataBag).to receive_message_chain('load.to_hash') { {} }
      end

      it 'return empty hash' do
        is_expected.to be_empty
      end
    end
  end

  describe '#find_local_data_bag_items' do
    subject do
      instance.find_local_data_bag_items("#{data_bag_path}/#{data_bag}", items)
    end

    context 'when data bag is not empty' do
      it 'return data bag items as hash' do
        is_expected.to eq(data_bag_as_hash)
      end
    end

    context 'when data bag is empty' do
      let(:data_bag) { 'empty' }

      it 'return empty hash' do
        is_expected.to be_empty
      end
    end
  end

  describe '#data_bag_item_from_chef' do
    subject { instance.data_bag_item_from_chef(data_bag, items.first) }

    context 'when data bag item is found' do
      before do
        allow(Chef::DataBagItem).to receive_message_chain('load.raw_data')
          .and_return(data_bag_as_hash[items.first])
      end

      it 'return data bag item as hash' do
        expect(subject).to eq(data_bag_as_hash['item1'])
      end
    end

    context 'when data bag item is not found' do
      before do
        allow(Chef::DataBagItem)
          .to receive('load')
          .with(data_bag, items.first)
          .and_raise(Net::HTTPServerException.new('stub', Net::HTTPNotFound))
      end

      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#data_bag_item_from_file' do
    subject do
      instance.data_bag_item_from_file(
        "#{data_bag_path}/#{data_bag}",
        "#{data_bag_path}/#{data_bag}/#{items.first}.json"
      )
    end

    context 'when data bag item is found' do
      it 'return data bag item as hash' do
        expect(subject).to eq(data_bag_as_hash['item1'])
      end
    end

    context 'when encrypted data bag item is found' do
      let(:items) { %w[encrypted] }

      it 'return unencrypted data bag item as hash' do
        expect(subject).to eq(data_bag_as_hash['item1'])
      end
    end

    context 'when data bag item is not found' do
      let(:data_bag) { 'absent' }

      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#compare' do
    context 'when items are present' do
      it 'call #compare_item for each item' do
        expect(instance).to receive('compare_item').twice
        instance.compare(data_bag_as_hash, data_bag_as_hash)
      end
    end

    context 'when items are missing' do
      it 'display error' do
        expect(instance.ui).to receive('error').twice
        instance.compare(data_bag_as_hash, {})
      end
    end
  end

  describe '#compare_item' do
    context 'when items are identical' do
      it 'display properly synchronized' do
        expect(instance).to receive('show_success')
          .with("Item '#{items.first}':".ljust(50))
        instance.compare_item(
          items.first,
          data_bag_as_hash.first,
          data_bag_as_hash.first
        )
      end
    end

    context 'when items are not identical' do
      it 'display difference(s) found' do
        expect(instance).to receive('show_error')
          .with("Item '#{items.first}':".ljust(50), anything)
        instance.compare_item(
          items.first,
          data_bag_as_hash.first,
          'id' => 'item1'
        )
      end
    end
  end

  describe '#decrypt' do
    subject do
      data = File.open("#{data_bag_path}/#{data_bag}/encrypted.json").read
      instance.decrypt(data_bag, JSON.parse(data))
    end

    context 'when data is encrypted and secret key given' do
      it 'return plain data' do
        is_expected.to eq(data_bag_as_hash['item1'])
      end
    end

    context 'when data is encrypted and secret key absent' do
      before do
        allow(instance).to receive(:secret).and_return(nil)
      end

      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        expect(subject).to be_nil
      end
    end
  end

  describe '#sync_data_bags' do
    context 'when both data bag are found' do
      before do
        allow(instance).to receive(:data_bag_from_chef)
          .with(data_bag).and_return(data_bag_as_hash)
        allow(instance).to receive(:data_bag_from_file)
          .with(data_bag, data_bag_as_hash.keys).and_return(data_bag_as_hash)
      end

      it 'call #compare' do
        expect(instance).to receive('compare')
          .with(data_bag_as_hash, data_bag_as_hash).once
        expect { instance.sync_data_bags([data_bag]) }
          .to output.to_stderr_from_any_process
      end
    end

    context 'when at least one data bag is missing' do
      before do
        allow(instance).to receive(:data_bag_from_chef)
          .with(data_bag).and_return(data_bag_as_hash)
        allow(instance).to receive(:data_bag_from_file)
          .with(data_bag, items).and_return({})
      end

      it 'do not call #compare' do
        expect(instance).to_not receive('compare')
        expect { instance.sync_data_bags([data_bag]) }
          .to output.to_stderr_from_any_process
      end
    end
  end

  describe '#sync_item' do
    context 'when both items are found' do
      before do
        allow(instance).to receive(:data_bag_item_from_chef)
          .with(data_bag, items.first)
          .and_return(data_bag_as_hash[items.first])
        allow(instance).to receive(:data_bag_from_file)
          .with(data_bag, [items.first]).and_return(data_bag_as_hash)
      end

      it 'call #compare_item' do
        expect(instance).to receive('compare_item').with(
          items.first,
          data_bag_as_hash[items.first],
          data_bag_as_hash[items.first]
        ).once
        expect { instance.sync_item(data_bag, items.first) }
          .to output.to_stderr_from_any_process
      end
    end

    context 'when at least one item is missing' do
      before do
        allow(instance).to receive(:data_bag_item_from_chef)
          .with(data_bag, items.first)
          .and_return(data_bag_as_hash[items.first])
        allow(instance).to receive(:data_bag_from_file)
          .with(data_bag, [items.first]).and_return({})
      end

      it 'do not call #compare_item' do
        expect(instance).to_not receive('compare_item').with(
          items.first,
          data_bag_as_hash[items.first],
          data_bag_as_hash[items.first]
        )
        expect { instance.sync_item(data_bag, items.first) }
          .to output.to_stderr_from_any_process
      end
    end
  end

  describe '#run' do
    context 'when only data bag is given' do
      before { instance.name_args = [data_bag] }

      it 'call #check_data_bags only once' do
        expect(instance).to receive('sync_data_bags').with([data_bag]).once
        instance.run
      end
    end

    context 'when data bag and item are given' do
      before do
        instance.name_args = [data_bag, items.first]
        allow(instance).to receive(:check_item)
          .with(data_bag, items.first)
      end

      it 'call #check_data_bags only once' do
        expect(instance).to receive('sync_item')
          .with(data_bag, items.first).once
        instance.run
      end
    end

    context 'when no arguments is given' do
      before do
        allow(Chef::DataBag).to receive(:list).and_return(data_bag => 'test')
        allow(instance).to receive(:check_data_bags)
          .with(data_bag, items.first)
      end

      it 'call #check_data_bags only once' do
        expect(instance).to receive('sync_data_bags')
          .with([data_bag]).once
        instance.run
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
