# frozen_string_literal: true

require 'spec_helper'

require 'chef/knife/core/object_loader'
require 'chef/knife/environment_sync'
require 'chef/environment'

# rubocop:disable Metrics/BlockLength
describe Chef::Knife::EnvironmentSync do
  let(:env) { 'test' }
  let(:instance) { described_class.new }

  Chef::Config[:environment_path] =
    [File.join(File.expand_path('..', __dir__), 'files', 'env')]

  describe '#env_from_chef' do
    subject { instance.env_from_chef(env) }

    context 'when environment is found' do
      before do
        allow(Chef::Environment).to receive('load').with(env) { env_as_hash }
      end

      it 'return environment as hash' do
        is_expected.to eq(env_as_hash)
      end
    end

    context 'when environment is not found' do
      before do
        allow(Chef::Environment)
          .to receive('load')
          .with(env)
          .and_raise(Net::HTTPServerException.new('stub', Net::HTTPNotFound))
      end

      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#env_from_file' do
    subject { instance.env_from_file(env) }

    context 'when environment is found' do
      it 'return environment as hash' do
        expect(subject).to eq(env_as_hash)
      end
    end

    context 'when environment is not found' do
      let(:env) { 'absent' }
      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#compare' do
    context 'when environments are identical' do
      it 'display properly synchronized' do
        expect(instance).to receive('show_success')
          .with("--> Checking #{env}".ljust(50))
        instance.compare(env, env_as_hash, env_as_hash)
      end
    end

    context 'when environments are not identical' do
      it 'display difference(s) found' do
        expect(instance).to receive('show_error')
          .with("--> Checking #{env}".ljust(50), anything)
        instance.compare(env, env_as_hash, {})
      end
    end
  end

  describe '#run' do
    let(:env1) { { 'name' => 'env1' } }
    let(:env2) { { 'name' => 'env2' } }

    context 'when single environment is given' do
      before do
        instance.name_args = [env]
        allow(instance).to receive(:env_from_chef)
          .with(env).and_return(env1)
        allow(instance).to receive(:env_from_file)
          .with(env).and_return(env2)
      end

      it 'call #compare only once' do
        expect(instance).to receive('env_from_chef').with(env).once
        expect(instance).to receive('env_from_file').with(env).once
        expect(instance).to receive('compare').with(env, env1, env2).once
        instance.run
      end
    end

    context 'when two environments are given' do
      before do
        instance.name_args = %w[test1 test2]
        allow(instance).to receive(:env_from_chef)
          .with(/test(1|2)/).and_return(env1)
        allow(instance).to receive(:env_from_file)
          .with(/test(1|2)/).and_return(env2)
      end

      it 'call #compare only twice' do
        expect(instance).to receive('env_from_chef').twice
        expect(instance).to receive('env_from_file').twice
        expect(instance).to receive('compare').twice
        instance.run
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
