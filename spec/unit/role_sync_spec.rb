# frozen_string_literal: true

require 'spec_helper'

require 'chef/knife/core/object_loader'
require 'chef/knife/role_sync'
require 'chef/role'
require 'hashdiff'
require 'find'

# rubocop:disable Metrics/BlockLength
describe Chef::Knife::RoleSync do
  let(:role) { 'test' }
  let(:instance) { described_class.new }

  Chef::Config[:role_path] =
    [File.join(File.expand_path('..', __dir__), 'files', 'role')]

  describe '#role_from_chef' do
    subject { instance.role_from_chef(role) }

    context 'when role is found' do
      before do
        allow(Chef::Role).to receive('load').with(role) { role_as_hash }
      end

      it 'return role as hash' do
        is_expected.to eq(role_as_hash)
      end
    end

    context 'when role is not found' do
      before do
        allow(Chef::Role)
          .to receive('load')
          .with(role)
          .and_raise(Net::HTTPServerException.new('stub', Net::HTTPNotFound))
      end

      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#role_from_file' do
    subject { instance.role_from_file(role) }

    context 'when role is found' do
      it 'return role as hash' do
        expect(subject).to eq(role_as_hash)
      end
    end

    context 'when role is not found' do
      let(:role) { 'absent' }
      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#find_role_file' do
    subject { instance.find_role_file(role) }

    context 'when file is found' do
      it 'return array not empty' do
        expect(subject).not_to be_empty
      end
    end

    context 'when file is not found' do
      let(:role) { 'absent' }
      it 'returns empty array' do
        expect(subject).to be_empty
      end
    end
  end

  describe '#compare' do
    context 'when roles are identicals' do
      it 'display properly synchronized' do
        expect(instance).to receive('show_success')
          .with("--> Checking #{role}".ljust(50))
        instance.compare(role, role_as_hash, role_as_hash)
      end
    end

    context 'when roles are not identicals' do
      it 'display difference(s) found' do
        expect(instance).to receive('show_error')
          .with("--> Checking #{role}".ljust(50), anything)
        instance.compare(role, role_as_hash, {})
      end
    end
  end

  describe '#run' do
    let(:role1) { { 'name' => 'role1' } }
    let(:role2) { { 'name' => 'role2' } }

    context 'when single role is given' do
      before do
        instance.name_args = [role]
        allow(instance).to receive(:role_from_chef)
          .with(role).and_return(role1)
        allow(instance).to receive(:role_from_file)
          .with(role).and_return(role2)
      end

      it 'call #compare only once' do
        expect(instance).to receive('role_from_chef').with(role).once
        expect(instance).to receive('role_from_file').with(role).once
        expect(instance).to receive('compare').with(role, role1, role2).once
        instance.run
      end
    end

    context 'when two roles are given' do
      before do
        instance.name_args = %w[test1 test2]
        allow(instance).to receive(:role_from_chef)
          .with(/test(1|2)/).and_return(role1)
        allow(instance).to receive(:role_from_file)
          .with(/test(1|2)/).and_return(role2)
      end

      it 'call #compare only twice' do
        expect(instance).to receive('role_from_chef').twice
        expect(instance).to receive('role_from_file').twice
        expect(instance).to receive('compare').twice
        instance.run
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
