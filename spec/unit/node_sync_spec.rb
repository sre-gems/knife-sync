# frozen_string_literal: true

require 'spec_helper'

require 'chef/knife/core/object_loader'
require 'chef/knife/node_sync'
require 'chef/node'

# rubocop:disable Metrics/BlockLength
describe Chef::Knife::NodeSync do
  let(:node) { 'test' }
  let(:instance) { described_class.new }

  Chef::Config[:node_path] =
    [File.join(File.expand_path('..', __dir__), 'files', 'node')]

  describe '#node_from_chef' do
    subject { instance.node_from_chef(node) }

    context 'when node is found' do
      before do
        allow(Chef::Node).to receive('load').with(node) { node_as_hash }
      end

      it 'return node as hash' do
        is_expected.to eq(node_as_hash)
      end
    end

    context 'when node is not found' do
      before do
        allow(Chef::Node)
          .to receive('load')
          .with(node)
          .and_raise(Net::HTTPServerException.new('stub', Net::HTTPNotFound))
      end

      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#node_from_file' do
    subject { instance.node_from_file(node) }

    context 'when node is found' do
      it 'return node as hash' do
        expect(subject).to eq(node_as_hash)
      end
    end

    context 'when node is not found' do
      let(:node) { 'absent' }
      it 'return nil and display error' do
        expect { subject }.to output.to_stderr_from_any_process
        is_expected.to be_nil
      end
    end
  end

  describe '#compare' do
    context 'when nodes are identical' do
      it 'display properly synchronized' do
        expect(instance).to receive('show_success')
          .with("--> Checking #{node}".ljust(50))
        instance.compare(node, node_as_hash, node_as_hash)
      end
    end

    context 'when nodes are not identical' do
      it 'display difference(s) found' do
        expect(instance).to receive('show_error')
          .with("--> Checking #{node}".ljust(50), anything)
        instance.compare(node, {}, node_as_hash)
      end
    end
  end

  describe '#run' do
    let(:node1) { { 'name' => 'node1' } }
    let(:node2) { { 'name' => 'node2' } }

    context 'when single node is given' do
      before do
        instance.name_args = [node]
        allow(instance).to receive(:node_from_chef)
          .with(node).and_return(node1)
        allow(instance).to receive(:node_from_file)
          .with(node).and_return(node2)
      end

      it 'call #compare only once' do
        expect(instance).to receive('node_from_chef').with(node).once
        expect(instance).to receive('node_from_file').with(node).once
        expect(instance).to receive('compare').with(node, node1, node2).once
        instance.run
      end
    end

    context 'when two nodes are given' do
      before do
        instance.name_args = %w[test1 test2]
        allow(instance).to receive(:node_from_chef)
          .with(/test(1|2)/).and_return(node1)
        allow(instance).to receive(:node_from_file)
          .with(/test(1|2)/).and_return(node2)
      end

      it 'call #compare only twice' do
        expect(instance).to receive('node_from_chef').twice
        expect(instance).to receive('node_from_file').twice
        expect(instance).to receive('compare').twice
        instance.run
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
